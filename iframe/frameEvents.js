
(function(window) {
    var $ = window.jQuery;

    $(window).on('paymentsuccess PaymentSuccess paymentSuccess Paymentsuccess', function() {
        window.frameDriver && window.frameDriver.send('paymentsuccess');
    });

    $(window).on('paymenterror PaymentError paymentError Paymenterror', function() {
        window.frameDriver && window.frameDriver.send('paymenterror');
    });

    $(window).on('3dsredirect', function(event, data) {
        window.frameDriver && window.frameDriver.send('3dsredirect', data);
    });

})(window);