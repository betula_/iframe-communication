

(function(window) {

    var checkOrigin = function(origin) {
        return /:[/]{2}(www.)?(mann-ivanov-ferber|m-i-f|mifdev).ru$/.exec(origin) && true;
    };

    var driver = {

        // EventEmitter

        listeners: {},
        emit: function(name, data) {
            var args = Array.prototype.slice.call(arguments, 1);
            if (!this.listeners[name]) return;
            var listeners = this.listeners[name].slice();
            for (var i = 0; i < listeners.length; i++) {
                listeners[i].apply(this, args);
            }
        },
        on: function(name, fn) {
            if (!this.listeners[name]) this.listeners[name] = [];
            this.listeners[name].push(fn);
            var emitter = this;
            return function() {
                emitter.off(name, fn);
            };
        },
        off: function(name, fn) {
            if (!this.listeners[name]) return;
            for (var i = 0; i < this.listeners[name].length; ) {
                if (this.listeners[name][i] === fn) {
                    this.listeners[name].splice(i, 1);
                } else ++i;
            }
        },

        // Connection

        connection: {
            source: null,
            origin: null,
            key: null,
            check: function() {
                return this.source && this.origin && this.key;
            }
        },

        register: function() {
            var self = this;
            window.addEventListener('message', function(event) {
                if (!checkOrigin(event.origin)) {
                    return;
                }
                try {
                    var message = JSON.parse(event.data || '');
                }
                catch (e) {
                    return;
                }
                if (!message || !(message instanceof Object)) {
                    return;
                }
                if (!message.action || typeof message.action != 'string' || !message.key || typeof message.key != 'string') {
                    return;
                }

                var conn = self.connection;
                conn.source = event.source;
                conn.origin = event.origin;
                conn.key = message.key;

                self.emit(message.action, message.data);
            });
        },

        queue: [],
        send: function(action, data) {
            var self = this;
            var conn = self.connection;
            if (document.cookie.indexOf('--js-debug-frame-messages') >= 0) {
                console.log('frame message send:', action, data, conn);
            }
            if (conn.check()) {
                self.queue.forEach(function(i) {
                    postMessage.apply(null, i);
                });
                self.queue.length = 0;
                postMessage(action, data);
            }
            else {
                try {
                    self.queue.push(JSON.parse(JSON.stringify([action, data])));
                } catch (e) {}
            }

            function postMessage(action, data) {
                var message = {};
                message[conn.key] = {
                    action: action,
                    data: data
                };
                try {
                    conn.source.postMessage(JSON.stringify(message), conn.origin);
                } catch (e) {}
            }
        }

    };

    driver.on('connect', function() {
        driver.send('ready');
    });

    driver.register();

    window.frameDriver = driver;

})(window);

