
(function(window) {
    var $ = window.jQuery;
    var RESIZE_TICKER_TIMEOUT = 334;

    var resizable = {

        width: null,
        height: null,

        getOuterRect: function() {
            var element = $(window);
            return {
                width: element.width(),
                height: element.height()
            }
        },
        getInnerRect: function() {
            var element = $('body');
            return {
                width: element.width(),
                height: element.height()
            }
        },

        bind: function() {
            var outer = this.getOuterRect();
            var inner = this.getInnerRect();
            if (outer.width != inner.width || outer.height != inner.height) {
                this.update();
            }
            this.registerResizeListener();
            this.registerResizeTicker();
        },

        registerResizeListener: function() {
            var self = this;
            $(window).on('resize', function() {
                self.update();
            });
        },

        registerResizeTicker: function() {
            var self = this;
            setInterval(function() {
                self.update();
            }, RESIZE_TICKER_TIMEOUT);
        },

        update: function() {
            if (!window.frameDriver) {
                return;
            }
            var rect = this.getInnerRect();
            var w = rect.width;
            var h = rect.height;
            if (w == this.width && h == this.height) {
                return;
            }
            window.frameDriver.send('resize', {
                width: parseInt(w),
                height: parseInt(h)
            });

            this.width = w;
            this.height = h;
        }

    };

    $(function() {
        resizable.bind();
    });

})(window);

