module.directive('cBookBuyPaymentRfiInline', ['jQuery', function(jQuery) {
	var copy = angular.copy;
	return {
		restrict: 'A',
		templateUrl: 'template:c-book-buy-payment-rfi-inline',
		scope: {
			params: '=paymentParams',
			success: '&paymentSuccess',
			error: '&paymentError',
			cancel: '&paymentCancel'
		},
		controller: ['$scope', '$http', '$q', '$element', function($scope, $http, $q, $element) {

			$scope.gate = {
				url: '',
				origins: [
					'https://secure.rficb.ru',
					'https://test.rficb.ru'
				],
				method: 'post',
				params: {},
				action: function(url, data) {
					if (!url) {
						return;
					}
					this.url = url;
					this.params = data || {};
					this.send();
				},
				send: function() {
					this.unready();
					setTimeout(function() {
						$element.find('form[form-paygate-frame]').trigger('submit');
					}, 0);
				},

				isAborted: false,
				abort: function() {
					if (this.isAborted) {
						return;
					}
					this.isAborted = true;
					$scope.error({
						$error: 'gateway.abort'
					});
				},

				error: function() {
					$scope.error({
						$error: 'payment.error'
					});
				},

				isReady: false,
				ready: function() {
					this.isReady = true;
				},
				unready: function() {
					this.isReady = false;
					this.isPaying = false;
				},

				orderId: null,
				orderWaiting: function() {
					return $http.post('/buy/waitorder.ajax', {orderid: this.orderId });
				},

				success: function() {

					this.isPaying = true;
					this.orderWaiting();

					var self = this;
					var TIMEOUT = 1000;
					var MAX_ATTEMPTS = 20;
					var attempts = 0;
					check();

					function attempt() {
						if (!self.isPaying) {
							return;
						}
						if (attempts >= MAX_ATTEMPTS) {
							$scope.error({
								$error: 'order.status.timeout'
							});
							self.isPaying = false;
							return;
						}
						attempts ++;
						setTimeout(function() {
							if (!self.isPaying) {
								return;
							}
							check();
						}, TIMEOUT);
					}
					function check() {
						var promise = $http.post('/buy/statusorder.ajax', { orderid: self.orderId });
						promise.then(function(response) {
							if (!self.isPaying) {
								return;
							}
							var data = response.data || {};
							if (data.status) {
								$scope.success();
								self.isPaying = false;
							}
							else {
								attempt();
							}
						}, function() {
							attempt();
						});
					}

				}
			};

			$scope.gate['3ds'] = {
				enabled: false,
				action: null,
				method: 'post',
				data: {},
				process: function(data) {
					this.action = data.action;
					this.method = data.method || this.method;
					this.data = data.data || this.data;
					this.enabled = true;
					setTimeout(function() {
						$element.find('form[form-3ds-redirect]').trigger('submit');
					}, 0);
				}
			};


			$scope.process = {
				pending: true,
				error: false,
				success: false,

				$loadTimeout: null,

				load: function() {
					if (this.$loadTimeout) {
						this.$loadTimeout.reject();
						this.$loadTimeout = null;
					}
					var params = $scope.params;
					var data = {
						productid: params.bookId,
						type: params.bookType,
						code: params.promoCode,
						method: 'Rfi'
					};
					var self = this;
					this.pending = true;
					this.$loadTimeout = $q.defer();
					var promise = $http.post('/buy/processorder.ajax', data, { timeout: this.$loadTimeout.promise });
					promise.finally(function() {
						self.pending = false;
					});
					promise.then(function(response) {
						self.success = true;
						self.error = false;

						var data = response.data || {};
						var rfi = data.Rfi || {};
						$scope.gate.orderId = data.orderId;
						$scope.gate.action(rfi.endpoint, rfi.data);

					}, function() {
						self.success = false;
						self.error = true;

						// Error
						$scope.error({
							$error: 'order.process.error'
						});
					});
				}
			};

			$scope.load = function() {
				$scope.process.load();
			};

		}],
		link: function(scope) {
			scope.load();
		}
	}
}]);