module.directive('iframeRfiPaymentSuccess', [function() {
	return {
		restrict: 'A',
		require: 'iframeCommunication',
		link: function(scope, element, attrs, comm) {
			var evaluated = false;
			comm.on('bind', function() {
				evaluated = false;
			});
			comm.on('paymentsuccess', function() {
				if (evaluated) {
					return;
				}
				evaluated = true;
				scope.$apply(function() {
					scope.$eval(attrs.iframeRfiPaymentSuccess);
				});
			});
		}
	}
}]);

module.directive('iframeRfiPaymentError', [function() {
	return {
		restrict: 'A',
		require: 'iframeCommunication',
		link: function(scope, element, attrs, comm) {
			var evaluated = false;
			comm.on('bind', function() {
				evaluated = false;
			});
			comm.on('paymenterror', function() {
				if (evaluated) {
					return;
				}
				evaluated = true;
				scope.$apply(function() {
					scope.$eval(attrs.iframeRfiPaymentError);
				});
			});
		}
	}
}]);

module.directive('iframeRfiPaymentReady', ['jQuery', function(jQuery) {
	return {
		restrict: 'A',
		require: 'iframeCommunication',
		link: function(scope, element, attrs, comm) {

			var unbind = null;
			comm.on('unbind', function() {
				unbind && unbind();
			});
			comm.on('bind', function() {
				unbind && unbind();
				var readyDeferred = jQuery.Deferred();
				var unbindReady = comm.on('ready', function() {
					readyDeferred.resolve();
				});
				var resizeDeferred = jQuery.Deferred();
				var unbindResize = comm.on('resize', function() {
					resizeDeferred.resolve();
				});

				unbind = function() {
					unbindReady();
					unbindResize();
				};

				jQuery.when(readyDeferred, resizeDeferred).done(function() {
					scope.$apply(function() {
						scope.$eval(attrs.iframeRfiPaymentReady);
					});
				});
			});

		}
	}
}]);

module.directive('iframeRfiPaymentAbort', ['jQuery', function(jQuery) {
	var TIMEOUT = 8000;

	return {
		restrict: 'A',
		require: 'iframeCommunication',
		link: function(scope, element, attrs, comm) {

			var abortDeferred = null;

			comm.on('ready', function() {
				if (abortDeferred) {
					abortDeferred.reject();
				}
			});

			comm.on('abort', function() {
				abortDeferred = jQuery.Deferred();
				var timeoutTimerId = setTimeout(function() {
					abortDeferred.resolve();
				}, TIMEOUT);
				var cancelTimeout = function() {
					clearTimeout(timeoutTimerId);
				};
				jQuery.when(abortDeferred).fail(function() {
					cancelTimeout();
				});

				jQuery.when(abortDeferred).then(function() {
					scope.$apply(function() {
						scope.$eval(attrs.iframeRfiPaymentAbort);
					});
					abortDeferred = null;
				}, function() {
					cancelTimeout();
					abortDeferred = null;
				});
			});

		}
	}
}]);

module.directive('iframeRfiPayment3dsRedirect', [function() {
	return {
		restrict: 'A',
		require: 'iframeCommunication',
		link: function(scope, element, attrs, comm) {
			comm.on('3dsredirect', function(data) {
				data = data || {};
				if (!data.action) {
					return;
				}
				data.method = data.method || 'post';
				data.data = data.data || {};
				scope.$apply(function() {
					scope.$eval(attrs.iframeRfiPayment3dsRedirect, {
						$data: data
					});
				});
			});
		}
	}
}]);

module.directive('iframeRfiPaymentUnready', [function() {
	return {
		restrict: 'A',
		require: 'iframeCommunication',
		link: function(scope, element, attrs, comm) {
			comm.on('unbind', function() {
				scope.$apply(function() {
					scope.$eval(attrs.iframeRfiPaymentUnready);
				});
			});
		}
	}
}]);