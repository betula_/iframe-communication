module.directive('iframeAutoresize', [function() {
	return {
		restrict: 'A',
		require: 'iframeCommunication',
		link: function(scope, element, attrs, comm) {
			comm.on('resize', function(data) {
				if (!data) {
					return;
				}
				if (data.width || data.height) {
					element.attr('scrolling', 'no');
				}
				if (data.width) {
					element.width(data.width);
				}
				if (data.height) {
					element.height(data.height);
				}
			});
		}
	}
}]);

module.directive('iframeAutoresizeWidth', [function() {
	return {
		restrict: 'A',
		require: 'iframeCommunication',
		link: function(scope, element, attrs, comm) {
			comm.on('resize', function(data) {
				if (!data) {
					return;
				}
				if (data.width) {
					element.attr('scrolling', 'no');
					element.width(data.width);
				}
			});
		}
	}
}]);

module.directive('iframeAutoresizeHeight', [function() {
	return {
		restrict: 'A',
		require: 'iframeCommunication',
		link: function(scope, element, attrs, comm) {
			comm.on('resize', function(data) {
				if (!data) {
					return;
				}
				if (data.height) {
					element.attr('scrolling', 'no');
					element.height(data.height);
				}
			});
		}
	}
}]);

module.directive('iframeAutoresizeWidthDefault', [function() {
	return {
		restrict: 'A',
		require: 'iframeCommunication',
		link: function(scope, element, attrs, comm) {
			comm.on('abort', function() {
				var width = attrs.iframeAutoresizeWidthDefault;
				if (!width) {
					return;
				}
				element.attr('scrolling', 'yes');
				element.width(width);
			});
		}
	}
}]);

module.directive('iframeAutoresizeHeightDefault', [function() {
	return {
		restrict: 'A',
		require: 'iframeCommunication',
		link: function(scope, element, attrs, comm) {
			comm.on('abort', function() {
				var height = attrs.iframeAutoresizeHeightDefault;
				if (!height) {
					return;
				}
				element.attr('scrolling', 'yes');
				element.height(height);
			});
		}
	}
}]);