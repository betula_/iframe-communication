module.directive('iframeCommunication', [function() {

	function isLogging() {
		return document.cookie.indexOf('--js-debug-iframe-communication') >= 0;
	}
	function log() {
		if (isLogging()) {
			console.log.apply(console, ['iframe communication'].concat(Array.prototype.slice.call(arguments)));
		}
	}

	return {
		restrict: 'A',
		controller: ['$scope', '$element', '$attrs', 'EventEmitterFactory', function($scope, $element, $attrs, EventEmitterFactory) {

			EventEmitterFactory.call(this);

			if (isLogging()) {
				(function(emit, self) {
					self.emit = function() {
						log.apply(null, ['emit'].concat(Array.prototype.slice.call(arguments)));
						emit.apply(self, arguments);
					}
				})(this.emit, this);
			}

			var getUrlHost = function(url, def) {
				return (/^[a-z0-9]+:[/]{2}[^/]+/.exec(url) || [])[0] || def || url;
			};
			var REGISTER_OWN_TARGET_TIMEOUT = 334;
			var MESSAGE_KEY = '::IFrameCommunication:5d41402abc4b2a76b9719d911017c592';
			var CONNECT_TIMEOUT = 3000;

			var realTargetOrigin = null;

			var unbindDelegate;
			this.unbind = function() {
				this.emit('unbind');
				unbindDelegate && unbindDelegate();
				unbindDelegate = null;
				realTargetOrigin = null;
			};

			var ownTarget = getUrlHost(window.location.href);
			this.bind = function() {
				this.emit('bind');

				var self = this;
				var ready = false;

				var connectTimeoutTimerId = setTimeout(function() {
					if (!ready) {
						self.unbind();
						self.emit('abort');
					}
				}, CONNECT_TIMEOUT);
				var cancelConnectTimeout = function() {
					clearTimeout(connectTimeoutTimerId);
				};

				connect();
				var unbindTargetOriginObserver = $attrs.$observe('iframeOrigin', function() {
					connect();
				});
				var unwatchTargetOrigins = $scope.$watchCollection($attrs.iframeOrigins, function() {
					connect();
				});
				var messageListener = function(event) {
					var message = event.data;
					try {
						message = (JSON.parse(message) || {})[MESSAGE_KEY];
						if (message && message.action && typeof message.action == 'string') {
							self.emit(message.action, message.data, event);
						}
					}
					catch (e) {}
				};

				window.addEventListener('message', messageListener);
				var unbindMessageListener = function() {
					window.removeEventListener('message', messageListener);
				};

				var stopConnectingAttempts;
				function connect() {
					if (ready) {
						return;
					}
					stopConnecting();
					var timerId = null;
					stopConnectingAttempts = function() {
						clearTimeout(timerId);
					};
					attempt();
					function attempt() {
						clearTimeout(timerId);
						timerId = setTimeout(function() {
							self.send('connect', ownTarget);
							attempt();
						}, REGISTER_OWN_TARGET_TIMEOUT);
					}
				}
				function stopConnecting() {
					stopConnectingAttempts && stopConnectingAttempts();
				}
				function finishConnecting() {
					ready = true;
					stopConnecting();
					cancelConnectTimeout();
				}
				var unbindReadyListener = self.on('ready', function(data, event) {
					realTargetOrigin = realTargetOrigin || event.origin;
					finishConnecting();
				});

				unbindDelegate = function() {
					cancelConnectTimeout();
					unbindTargetOriginObserver();
					unwatchTargetOrigins();
					unbindMessageListener();
					stopConnecting();
					unbindReadyListener();
				};
			};

			var getTargetOrigins = function() {
				var origins = [];
				if ($attrs.iframeOrigin) {
					origins.getUrlHost($attrs.iframeOrigin, ownTarget);
				}
				if ($attrs.iframeOrigins) {
					var list = $scope.$eval($attrs.iframeOrigins);
					if (list && list instanceof Array && list.length > 0) {
						Array.prototype.push.apply(origins, list);
					}
				}
				return origins;
			};
			var getRealTargetOrigin = function() {
				return realTargetOrigin || null;
			};

			this.send = function(action, data) {
				log('send', action, data);
				var window = $element[0].contentWindow;
				if (window && window.postMessage) {
					var postMessage = function(targetOrigin) {
						try {
							window.postMessage(JSON.stringify({
								action: action,
								data: data,
								key: MESSAGE_KEY
							}), targetOrigin);
						}
						catch (e) {}
					};

					var realTargetOrigin = getRealTargetOrigin();
					if (realTargetOrigin) {
						postMessage(realTargetOrigin);
					}
					else {
						getTargetOrigins().forEach(postMessage);
					}
				}
			};

		}],
		link: function(scope, element, attrs, ctrl) {
			element.on('load', function() {
				ctrl.unbind();
				ctrl.bind();
			});
		}
	}
}]);